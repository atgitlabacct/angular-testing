'use strict';

describe('Testing the test service', function() {
    var $injector;

    beforeEach(function() {
        module('angtest.services');
        inject(function(_$injector_) {
            $injector = _$injector_;
        });
    });

    it('returns the correct string', inject(function(testService) {
        expect(testService.run()).toEqual('Test Service');
    }));

    it('returns the correct string', function() {
        var testService = $injector.get('testService');
        expect(testService.run()).toEqual('Test Service');
    });
});
