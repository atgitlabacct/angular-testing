'use strict';


describe('A test on controllers', function() {
    var scope, mod;

    beforeEach(function() {
        mod = module('angtest.controllers', function ($provide) {
                //$provide.value('testService', { run: function() { return 'Foo Bar'; }});
            });
    });

    it('set mod to undefined', function() {
        expect(mod).toBeUndefined();
    });

    it('sets the name correctly', inject(function($controller, $rootScope) {
        var $scope = $rootScope.$new();
        var controller = $controller('TestController', 
                { 
                    $scope: $scope,
                    testService: { run: function() { return 'bar'; } }
                });
        expect($scope.name).toEqual('bar');
    }));

    /*
     * Lets use inline array annotation for Dependency Annotations
     *
     * In tests we really don't have to do this simply because
     * we are not going to be minifying the code
     *
     * Notice how the function take c "function(c)" instead of $controller.
     */
    it('sets the name correctly', inject(['$controller', function(c) {
        var $scope = {};
        var controller = c('TestController', 
                {
                    $scope: $scope, 
                    testService: { run: function() { return 'foo'; } } 
                });
        expect($scope.name).toEqual('foo');
    }]));
});
