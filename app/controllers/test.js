'use strict';

angular.module('angtest.controllers').controller('TestController', ['$scope', 'testService',
        function TestController($scope, testService) {
            $scope.name = testService.run();
        }
]);

