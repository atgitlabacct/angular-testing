'use strict';

var TestService = function() {
    this.run = function() { return 'new TestService()'; };
};

angular.module('angtest.services').factory('testService', function () {
    return {
        run: function() { return 'Test Service'; }
    };
});

angular.module('angtest.services').service('newTestService', TestService);
